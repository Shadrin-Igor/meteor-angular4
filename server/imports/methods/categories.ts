import { Meteor } from 'meteor/meteor';

import { Categories } from '../../../imports/collections/categories';

Meteor.methods({
  addCategory(content: string) {
    Categories.insert({
      content
    });
  },
  removeCategory(_id: string) {
    Categories.remove({
      _id
    })
  }
})
