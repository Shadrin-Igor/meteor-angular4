import { Meteor } from 'meteor/meteor';

import { Products } from '../../../imports/collections/products';

Meteor.methods({
  addProducts(data: {content: string, category: string}) {
    console.log('data', data);
    Products.insert(data);
  },
  removeProducts(_id: string) {
    Products.remove({
      _id
    })
  }
})
