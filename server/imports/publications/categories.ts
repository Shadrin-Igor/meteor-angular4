import { Meteor } from 'meteor/meteor';

import { Categories } from '../../../imports/collections/categories';

Meteor.publish('categories', () => {
  return Categories.find({});
});

Meteor.publish('getCategory', (categoryID) => {
  return Categories.find({_id: categoryID});
});
