import { Meteor } from 'meteor/meteor';

import { Products } from '../../../imports/collections/products';

Meteor.publish('products', () => {
  return Products.find({});
});

