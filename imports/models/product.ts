export interface Product {
  _id?: string;
  content?: string;
  category?: string;
}
