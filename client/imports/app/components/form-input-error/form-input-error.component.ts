import {Component, Input, OnChanges, OnInit} from '@angular/core';

@Component({
  selector: 'app-form-input-error',
  templateUrl: './form-input-error.html',
  styleUrls: ['./form-input-error.scss']
})
export class FormInputErrorComponent implements OnInit, OnChanges {
  @Input() errors = {};
  @Input() field = '';
  listErrors: String[] = [];

  constructor() {
  }

  ngOnInit() {
    this.parseErrors();
  }

  ngOnChanges() {
    this.parseErrors();
  }

  private parseErrors() {
    let needError = true;
    this.listErrors = [];
    if (this.errors) {
      Object.keys(this.errors).forEach((key) => {
        if (needError) {
          const errorText = this.field ? `${this.field}-${key}` : key;
          this.listErrors.push(errorText);
          if (key === 'required') {
            needError = false;
          }
        }
      });
    }
  }
}
