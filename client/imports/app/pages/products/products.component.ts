import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Meteor } from 'meteor/meteor';
import { MeteorObservable } from 'meteor-rxjs';

import { Products } from '../../../../../imports/collections/products';
import { Categories } from '../../../../../imports/collections/categories';
import { Product } from '../../../../../imports/models/product';
import { Category } from '../../../../../imports/models/categoryt';

@Component({
  selector: 'products',
  templateUrl: 'products.html',
  styleUrls: ['products.scss']
})
export class ProductsComponent implements OnInit, OnDestroy {
  products: Observable<Products[]>;
  categories: Observable<Category[]> = [];
  subs: Subscription[] = [];
  ngOnInit() {
    const sub = MeteorObservable.subscribe('products').subscribe(() => {
      this.products = Products.find();
    });
    this.subs.push(sub);
    const sub2 = MeteorObservable.subscribe('categories').subscribe(() => {
      Categories.find().
        subscribe(data => {
          this.categories = data;
        });
    });
    this.subs.push(sub2);
  }
  ngOnDestroy() {
    if (this.subs.length) {
      this.subs.forEach(item => {
        item.unsubscribe();
      });
    }
  }
  remove(_id: string) {
    Meteor.call('removeProducts', _id);
  }
  getCategory(categoryId: string) {
    if (this.categories.length) {
      const item = this.categories.filter(item => item._id === categoryId);
      return item.length ? item[0].content : '';    
    }
     else return '';
  }
}
