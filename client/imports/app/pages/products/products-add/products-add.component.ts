import { Component } from '@angular/core';
import {Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { MeteorObservable } from 'meteor-rxjs';
import { Meteor } from 'meteor/meteor';

import { Categories } from '../../../../../../imports/collections/categories';
import { Category } from '../../../../../../imports/models/category';

@Component({
  selector: 'products-add',
  templateUrl: 'products-add.html',
  styleUrls: ['./products-add.scss']
})
export class ProductsAddComponent implements OnInit {
	content: string;
	isLoading = false;
	form: FormGroup;

	categories: Observable<Category[]>;
	categoryListSubscription: Subscription;

	constructor(private router: Router) {
	}

	ngOnInit() {
		this.categoryListSubscription = MeteorObservable.subscribe('categories').subscribe(() => {
		  this.categories = Categories.find();
		});

		this.form = new FormGroup({
			name: new FormControl(null, [Validators.required]),
			category: new FormControl(null, [Validators.required])
		});
	}

	ngOnDestroy() {
		if (this.categoryListSubscription) {
		  this.categoryListSubscription.unsubscribe();
		}
	}

	onSubmit() {
		if ( !this.form.invalid ) {
			Meteor.call('addProducts', this.form.value);
			this.form.reset();
			this.router.navigate(['/products']);
		}
	}
}
}
