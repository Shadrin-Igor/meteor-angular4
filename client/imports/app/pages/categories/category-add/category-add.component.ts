import { Component } from '@angular/core';
import {Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';

import { Meteor } from 'meteor/meteor';

@Component({
  selector: 'category-add',
  templateUrl: 'category-add.html',
  styleUrls: ['./category-add.scss']
})
export class CategoryAddComponent implements OnInit {
	content: string;
	isLoading = false;
	form: FormGroup;

	constructor(private router: Router) {

	}

	ngOnInit() {
		this.form = new FormGroup({
			name: new FormControl(null, [Validators.required])
		});
	}

	onSubmit() {
		if ( !this.form.invalid ) {
			Meteor.call('addCategory', this.form.value.name);
			this.form.reset();
			this.router.navigate(['/categories']);
		}
	}
}
}
