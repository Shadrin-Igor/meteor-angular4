import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Meteor } from 'meteor/meteor';
import { MeteorObservable } from 'meteor-rxjs';

import { Categories } from '../../../../../imports/collections/categories';
import { Category } from '../../../../../imports/models/category';

@Component({
  selector: 'categories',
  templateUrl: 'categories.html',
  styleUrls: ['categories.scss']
})
export class CategoriesComponent implements OnInit, OnDestroy {
  categories: Observable<Category[]>;
  categoryListSubscription: Subscription;
  ngOnInit() {
    this.categoryListSubscription = MeteorObservable.subscribe('categories').subscribe(() => {
      this.categories = Categories.find();
    });
  }
  ngOnDestroy() {
    if (this.categoryListSubscription) {
      this.categoryListSubscription.unsubscribe();
    }
  }
  remove(_id: string) {
    Meteor.call('removeCategory', _id);
  }
}
