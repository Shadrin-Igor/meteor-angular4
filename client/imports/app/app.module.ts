import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { CategoriesComponent } from './pages/categories/categories.component';
import { CategoryAddComponent } from './pages/categories/category-add/category-add.component';
import { ProductsComponent } from './pages/products/products.component';
import { ProductsAddComponent } from './pages/products/products-add/products-add.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { FormInputErrorComponent } from './components/form-input-error/form-input-error.component';
import { PreloaderComponent } from './components/preloader/preloader.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      {
        path: 'categories',
        component: CategoriesComponent
      },
      {
        path: 'categoryAdd',
        component: CategoryAddComponent
      },    
      {
        path: 'products',
        component: ProductsComponent
      },
      {
        path: 'productsAdd',
        component: ProductsAddComponent
      }, 
      {
        path: '**',
        component: PageNotFoundComponent
      }
    ])
  ],
  declarations: [
    AppComponent,
    CategoriesComponent,
    CategoryAddComponent,
    PageNotFoundComponent,
    FormInputErrorComponent,
    PreloaderComponent,
    ProductsComponent,
    ProductsAddComponent
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
